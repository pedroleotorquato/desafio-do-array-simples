# Desafio do array simples
### A solução do desafio se encontra no arquivo desafio_array_simples.js

O arquivo resposta (_desafio_array_simples.js_) é acompanhado de uma página (_index.html_) para facilitar sua correção.
**O arquivo resposta pode ser manipulado por meio do console do navegador :)** 

Decidi utilizar JavaScript na solução desse problema, pois o usuário hipotético seria um programador front-end, logo deve ter familiaridade com a linguagem.