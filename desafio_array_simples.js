var cotacoes = new Array

/* Adiciona um novo item ao formulario se o item ainda nao existe.
O titulo do novo item deve ser passado como parametro da funcao.*/
function adicionarCotacao(titulo)
{
    if(cotacoes.indexOf(titulo) == -1)
        cotacoes.push(titulo)
    else
        throw "Nao foi possivel adicionar a cotacao, pois esta ja consta no formulario."    
}

// Remove um item existente do formulario. O titulo do item a ser removido deve ser passado como parametro da funcao.
function removerCotacao(titulo)
{
    cotacoes.splice(verificaIndex(titulo), 1)    
}

/* Altera o titulo de um item existente do formulario. O titulo do item a ser alterado e 
o novo titulo devem ser passados como parametro da funcao, nesta ordem.
*/
function alterarTituloCotacao(titulo, novoTitulo)
{
    cotacoes[verificaIndex(titulo)] = novoTitulo
}

/* Funcao auxiliar para verificar a existencia de um elemento e retornar
seu respectivo index no array simples. Essa funcao nao precisa ser utilizada por voce, Pedro :)
*/
function verificaIndex(titulo)
{
    var index = cotacoes.indexOf(titulo)
    if(index == -1)
    {
        throw "Cotacao inexistente"
    }else
    {
        return index
    }
}